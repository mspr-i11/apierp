package com.payetonkawa.erpapi;

import com.payetonkawa.erpapi.controller.ProductController;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class productControllerTest {

    @Test
    public void testHealthEndpoint() throws Exception {
        // Créer une instance de HealthController
        ProductController controller = new ProductController();

        // Appeler la méthode qui gère la route "/api/health"
        assertNotNull(controller.getAllProduct());

    }
}
