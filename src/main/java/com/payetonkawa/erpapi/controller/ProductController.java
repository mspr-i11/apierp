package com.payetonkawa.erpapi.controller;


import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/api/products")
public class ProductController {

    String url = "https://615f5fb4f7254d0017068109.mockapi.io/api/v1/products/";

    @GetMapping
    public List<Object> getAllProduct() {
        RestTemplate rt = new RestTemplate();

        Object[] products = rt.getForObject(url, Object[].class);

        return Arrays.asList(products);
    }
    @GetMapping("/{id}")
    public Object getProduct(@PathVariable int id){
        RestTemplate rt = new RestTemplate();
        String urlCusto = url + id;
        Object products = rt.getForObject(urlCusto, Object.class);

        return products;
    }

}
